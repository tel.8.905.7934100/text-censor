from ahocorasick_rs import AhoCorasick, MatchKind
from glob import glob
import re
import string

def remove_punctuation(input_string):
    # Make a regular expression that matches all punctuation
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    # Use the regex
    return regex.sub('', input_string)

def load_censor_txt(folder='.') :
    dct = []
    for f in glob(folder+'/*.txt') :
        dct+=open(f).read().lower().split('\n')
    return dct

txts = ["мачта корабля","фейхуёвая чача в херсоне","пиздец", "bloody rain"]

cdict = load_censor_txt()

for str in txts :
    ac = AhoCorasick(cdict, matchkind=MatchKind.LeftmostLongest)
    cens = ac.find_matches_as_strings(str)
    print(len(cens),'\n',cens)
    if len(cens) > 0 :
        print(cens.index(cens[0]))
